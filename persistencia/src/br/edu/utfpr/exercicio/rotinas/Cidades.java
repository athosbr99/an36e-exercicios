package br.edu.utfpr.exercicio.rotinas;

public enum Cidades {
    Londrina,
    Cambé,
    Ibiporã,
    Assis,
    Assaí,
    Rolândia,
    Bauru,
    Curitiba,
    Jataizinho,
    Jacarézinho,
    Mauá,
    Florianópolis,
    Franca,
    Petropolis,
    Contagem,
    Manaus,
    Cajamar,
    Maceió,
    Sertanópolis,
    Uraí
}
