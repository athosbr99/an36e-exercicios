package br.edu.utfpr.exercicio.rotinas;

import br.edu.utfpr.exercicio.crud.CrudCidade;
import br.edu.utfpr.exercicio.entity.Cidade;

public class InserirCidades {
    
    public static void inserir() {
        /*
       
        Se não tiver nenhuma cidade cadastrada, preenche a lista de cidades com valores do enum Cidades.
        
        */
        Cidade cid = new CrudCidade().find(Integer.parseInt("1"));
        Cidades[] b = Cidades.values();
        if (cid == null) {
            for (int i = 0; i < b.length; i++) {
                cid = new Cidade();
                cid.setNome(b[i].name());
                cid.setCodigo(i+1);
                new CrudCidade().persist(cid);
            }
        }
    }    
}
