package br.edu.utfpr.exercicio.rotinas;

import br.edu.utfpr.exercicio.crud.CrudCliente;
import br.edu.utfpr.exercicio.crud.CrudCidade;
import br.edu.utfpr.exercicio.entity.Cidade;
import br.edu.utfpr.exercicio.entity.Cliente;
import java.util.List;
import java.util.Random;

public class Rotina1 implements Runnable {
    
    /*
    
    Insere um numero de pessoas no banco de dados.
    Os nomes e sobrenomes estão salvos nos enums respectivos e são escolhidos aleatoriamente,
    assim como a idade.
    
    */
    
    int num_inserts = 0;
    
    public Rotina1(int num_in) {
        this.num_inserts = num_in;
    }
    
    private static synchronized void inserirPessoas(int num_cli) {
      Random rand = new Random();
      List<Cidade> lcid = new CrudCidade().getAll();
      Nomes[] nomes = Nomes.values();
      Sobrenomes[] sobrenomes = Sobrenomes.values();
      Cliente cli = new CrudCliente().find(Integer.parseInt("1"));
      if (cli == null) {
          for (int i = 0; i < num_cli; i++) {
              cli = new Cliente();
              cli.setIdade(rand.nextInt(50) + 20);
              cli.setCodigo(i+1);
              cli.setNome(nomes[rand.nextInt(100)].name());
              cli.setSobrenome(sobrenomes[rand.nextInt(100)].name());
              cli.setCidade(lcid.get(rand.nextInt(20)));
              new CrudCliente().persist(cli);
          }
      }
    }
    
    @Override
    public void run() {
        System.out.println("Thread iniciada.");
        inserirPessoas(num_inserts);
    }
}
