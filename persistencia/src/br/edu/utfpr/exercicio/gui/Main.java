package br.edu.utfpr.exercicio.gui;

import br.edu.utfpr.exercicio.crud.CrudCidade;
import br.edu.utfpr.exercicio.crud.CrudCliente;
import br.edu.utfpr.exercicio.entity.Cidade;
import br.edu.utfpr.exercicio.entity.Cliente;
import br.edu.utfpr.exercicio.rotinas.InserirCidades;
import br.edu.utfpr.exercicio.rotinas.Rotina1;
import java.util.List;
import javax.swing.JOptionPane;

public class Main extends javax.swing.JFrame {

    public Main() {
        InserirCidades.inserir();
        initComponents();
    }
    
    protected void execRotina1() {
        List<Cliente> lcli = new CrudCliente().getAll();
        if (!lcli.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Você já executou a Rotina 1.", "Erro", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                int num_inserts = Integer.parseInt(JOptionPane.showInputDialog(this, "Insira a quantidade de inserts que você deseja", JOptionPane.INFORMATION_MESSAGE));
                int num_threads = Integer.parseInt(JOptionPane.showInputDialog(this, "Insira a quantidade de threads que você deseja", JOptionPane.INFORMATION_MESSAGE));
                for (int i = 0; i < num_threads; i++) {
                    Thread t = new Thread(new Rotina1(num_inserts));
                    t.start();
                }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Insira numeros inteiros.", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAddCidade = new javax.swing.JButton();
        txtCod = new javax.swing.JTextField();
        txtNom = new javax.swing.JTextField();
        btnAtualizarListaCidades = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtList = new javax.swing.JTextArea();
        btnUpdateCidade = new javax.swing.JButton();
        btnDeletarCidade = new javax.swing.JButton();
        txtCod2 = new javax.swing.JTextField();
        txtNom2 = new javax.swing.JTextField();
        txtCod3 = new javax.swing.JTextField();
        btnRotina1 = new javax.swing.JButton();
        btnAtualizarClientes = new javax.swing.JButton();
        btnRotina2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnAddCidade.setText("Adicionar cidade");
        btnAddCidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddCidadeActionPerformed(evt);
            }
        });

        btnAtualizarListaCidades.setText("Atualizar lista de cidades");
        btnAtualizarListaCidades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarListaCidadesActionPerformed(evt);
            }
        });

        txtList.setColumns(20);
        txtList.setRows(5);
        jScrollPane1.setViewportView(txtList);

        btnUpdateCidade.setText("Atualizar cidade");
        btnUpdateCidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateCidadeActionPerformed(evt);
            }
        });

        btnDeletarCidade.setText("Deletar cidade");
        btnDeletarCidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeletarCidadeActionPerformed(evt);
            }
        });

        txtCod2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCod2ActionPerformed(evt);
            }
        });

        txtCod3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCod3ActionPerformed(evt);
            }
        });

        btnRotina1.setText("Rotina 1");
        btnRotina1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRotina1ActionPerformed(evt);
            }
        });

        btnAtualizarClientes.setText("Atualizar lista de clientes");
        btnAtualizarClientes.setToolTipText("");
        btnAtualizarClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarClientesActionPerformed(evt);
            }
        });

        btnRotina2.setText("Rotina 2");
        btnRotina2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRotina2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnAtualizarListaCidades, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnAddCidade, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnUpdateCidade, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCod2)
                            .addComponent(txtCod, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNom)
                            .addComponent(txtNom2)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnRotina1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRotina2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnAtualizarClientes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnDeletarCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtCod3, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddCidade))
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdateCidade)
                    .addComponent(txtCod2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNom2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAtualizarListaCidades)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAtualizarClientes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDeletarCidade)
                    .addComponent(txtCod3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRotina1)
                    .addComponent(btnRotina2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 93, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAtualizarListaCidadesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarListaCidadesActionPerformed
        List<Cidade> lcid = new CrudCidade().getAll();
        String txt = "";
        for (Cidade cidade : lcid) {
            txt = txt + cidade.getCodigo();
            txt = txt + " | ";
            txt = txt + cidade.getNome();
            txt = txt + " \n ";
        }
        txtList.setText(txt);
        txtNom2.setText("");
        txtNom.setText("");
        txtCod.setText("");
        txtCod2.setText("");
        txtCod3.setText("");
    }//GEN-LAST:event_btnAtualizarListaCidadesActionPerformed

    private void btnUpdateCidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateCidadeActionPerformed
        Cidade cid = new CrudCidade().find(Integer.parseInt(txtCod2.getText()));
        if (cid!=null) {
            cid.setNome(txtNom2.getText());
            new br.edu.utfpr.exercicio.crud.CrudCidade().merge(cid);
        }
        txtCod2.setText("");
        txtNom2.setText("");
    }//GEN-LAST:event_btnUpdateCidadeActionPerformed

    private void btnAddCidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddCidadeActionPerformed
        Cidade cid = new Cidade();
        cid.setNome(txtNom.getText());
        cid.setCodigo(Integer.parseInt(txtCod.getText()));
        new br.edu.utfpr.exercicio.crud.CrudCidade().persist(cid);
        txtNom.setText("");
        txtCod.setText("");
    }//GEN-LAST:event_btnAddCidadeActionPerformed

    private void btnDeletarCidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeletarCidadeActionPerformed
        Cidade cid = new CrudCidade().find(Integer.parseInt(txtCod3.getText()));
        if(cid!=null){          
            new br.edu.utfpr.exercicio.crud.CrudCidade().remove(cid);
        }
        txtCod3.setText("");
    }//GEN-LAST:event_btnDeletarCidadeActionPerformed

    private void txtCod2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCod2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCod2ActionPerformed

    private void txtCod3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCod3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCod3ActionPerformed

    private void btnRotina1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRotina1ActionPerformed
        execRotina1();
    }//GEN-LAST:event_btnRotina1ActionPerformed

    private void btnAtualizarClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarClientesActionPerformed
        List<Cliente> lcli = new CrudCliente().getAll();
        String txt="";
        for (Cliente c : lcli) {
            txt = txt + c.getCodigo();
            txt = txt + " | ";
            txt = txt + c.getNome();
            txt = txt + " " + c.getSobrenome();
            txt = txt + " \n ";
        }
        txtList.setText(txt);
        txtNom2.setText("");
        txtNom.setText("");
        txtCod.setText("");
        txtCod2.setText("");
        txtCod3.setText("");
    }//GEN-LAST:event_btnAtualizarClientesActionPerformed

    private void btnRotina2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRotina2ActionPerformed
        /*
            Busca um cliente pelo seu ID.
            Se a tabela de clientes estiver vazia, irá executar a rotina 1 com seus parametros.
        */
        
        List<Cliente> lcli = new CrudCliente().getAll();
        Cliente cli = null;
        if (lcli.isEmpty()) {
              JOptionPane.showMessageDialog(this, "A Rotina 1 não foi executada. Ela será executada junto com a Rotina 2.", "Aviso", JOptionPane.WARNING_MESSAGE);
              execRotina1();
        } else {
            String busca = JOptionPane.showInputDialog(this, "Insira o ID de quem você deseja buscar.", "Busca");
            try {
                  cli = new CrudCliente().find(Integer.parseInt(busca));
                  if (cli == null) {
                      JOptionPane.showMessageDialog(this, "Não foram encontrados resultados para sua busca.", "Aviso", JOptionPane.WARNING_MESSAGE);
                  } else {
                      txtList.setText("Encontrado: " + cli.getNome() + " " + cli.getSobrenome());
                  }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Insira um numero.", "Erro", JOptionPane.ERROR_MESSAGE);
            }

        } 
    }//GEN-LAST:event_btnRotina2ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddCidade;
    private javax.swing.JButton btnAtualizarClientes;
    private javax.swing.JButton btnAtualizarListaCidades;
    private javax.swing.JButton btnDeletarCidade;
    private javax.swing.JButton btnRotina1;
    private javax.swing.JButton btnRotina2;
    private javax.swing.JButton btnUpdateCidade;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txtCod;
    private javax.swing.JTextField txtCod2;
    private javax.swing.JTextField txtCod3;
    private javax.swing.JTextArea txtList;
    private javax.swing.JTextField txtNom;
    private javax.swing.JTextField txtNom2;
    // End of variables declaration//GEN-END:variables
}
