package br.edu.utfpr.exercicio.crud;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class CrudCidade extends AbstractCrud<br.edu.utfpr.exercicio.entity.Cidade> {

    private EntityManager em;
    private static final String PU = EMNames.EMN1;

    public CrudCidade() {
        super(br.edu.utfpr.exercicio.entity.Cidade.class);
    }

    public List<br.edu.utfpr.exercicio.entity.Cidade> SelectByNome(String nome) {
        try {
            return getEntityManager().createNamedQuery("Teste.findByNome").setParameter("nome", "%" + nome.toUpperCase() + "%").getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = Persistence.createEntityManagerFactory(PU).createEntityManager();
        }
        return em;
    }
}