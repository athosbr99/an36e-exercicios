package br.edu.utfpr.exercicio.crud;

import br.edu.utfpr.exercicio.entity.Cliente;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class CrudCliente extends AbstractCrud<br.edu.utfpr.exercicio.entity.Cliente> {

    private EntityManager em;
    private static final String PU = EMNames.EMN1;
    
    public CrudCliente() {
        super(Cliente.class);
    }
    
    public List<Cliente> selectByNome(String nome) {
        try {
            return getEntityManager().createNamedQuery("Cliente.findByNome").setParameter("nome", "%" + nome.toUpperCase() + "%").getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = Persistence.createEntityManagerFactory(PU).createEntityManager();
        }
        return em;
    }
}