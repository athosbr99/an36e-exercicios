package org.athoscastro;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.faces.application.FacesMessage;
@FacesValidator("org.athoscastro.ValidadorPrimo")
public class Validador implements Validator {
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String texto = value.toString();
        int numero, cont = 0;
        FacesMessage msg;
        try{
            numero = Integer.parseInt(texto);
            if (numero > 10) {
                for (int i = 1; i < numero; i++) {
                    if ((numero % i) == 0) {
                        cont++;
                    }
                }
                if (cont == 1) {
                   msg = new FacesMessage("Numero primo.");
                } else {
                   msg = new FacesMessage("Numero não primo.");
                }  
            } else {
                msg = new FacesMessage("Numero menor que 10");
            }
        } catch (NumberFormatException e) {
           msg = new FacesMessage("Não inseriu numero."); 
        }
        throw new ValidatorException(msg);
    }
}
