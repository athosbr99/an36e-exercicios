package org.athoscastro;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Soma extends HttpServlet {
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
      /*
        
        Identifica se existe uma sessão criada e pega o valor do atributo SOMA_SESSAO
        para a variavel local. Se não existe sessão, é incializado com zero.
        
        Identifica se o numero inserido na página anterior é um numero. Se não é, a variavel
        é atribuida com o numero 0.
        
      */
          
      HttpSession sessao = request.getSession(true);
      ServletContext aplicacao = request.getSession(true).getServletContext();
      int soma_sessao = 0;
      int numero_usuario;
      int resultado_soma_sessao;
      int soma_aplicacao = 0;
      int resultado_soma_aplicacao;
      
      try {
        numero_usuario = Integer.parseInt(request.getParameter("numeroUsuario"));
      } catch (NumberFormatException e) {
        numero_usuario = 0;
      }
      
      if (sessao.getAttribute("SOMA_SESSAO") != null) {
        soma_sessao = (int) sessao.getAttribute("SOMA_SESSAO");
      } else {
        sessao.setAttribute("SOMA_SESSAO", soma_sessao);  
      }
      
      if (aplicacao.getAttribute("SOMA_APLICACAO") != null){
        soma_aplicacao = (int) aplicacao.getAttribute("SOMA_APLICACAO");  
      } else {
        aplicacao.setAttribute("SOMA_APLICACAO", soma_aplicacao);
      }
      
      /*
      
      Soma feita com as variaveis de sessão e de aplicação.
      Atribuição feita para salvar os valores na sessão e na aplicação.
      
      */
      resultado_soma_aplicacao = soma_aplicacao+numero_usuario;
      resultado_soma_sessao = soma_sessao+numero_usuario;
      aplicacao.setAttribute("SOMA_APLICACAO", resultado_soma_aplicacao);
      sessao.setAttribute("SOMA_SESSAO", resultado_soma_sessao);
      
      /*
      
      Código HTML para ser exibido na página.
      
      */
      
      response.setContentType("text/html; charset=UTF-8");
      response.setCharacterEncoding("UTF-8");
      PrintWriter out = response.getWriter();
      out.println("<!DOCTYPE html>\n" +
                  "<html>\n" +
                  "    <head>\n" +
                  "        <title>Sessão: Somatória</title>\n" +
                  "        <meta charset=\"UTF-8\">\n" +
                  "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                  "    </head>\n" +
                  "    <body><center>\n" +
                  "    <p> Numero inserido: " + numero_usuario + "</p><br>\n" + 
                  "        <p>Numero total para a sessão: " + resultado_soma_sessao +"</p>\n" +
                  "        <p>Numero total para a aplicação: " + resultado_soma_aplicacao +"</p>\n" +       
                  "    </center></body>\n" +
                  "</html>\n" +
                  "");
    }
}