# Servlets: Sessão

Projeto para demonstrar uso de variaveis de sessão em Servlets.

O projeto consiste em uma aplicação web em Servlets que possui duas variaveis com um numero. Uma das variaveis deve ser utilizada para todos os usuarios do sistema, enquanto uma somente pro usuário que está acessando o sistema atualmente.
